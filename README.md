**This repository contains the files we used to preprocessed raw fastq files on kubernetes cluster using argo workflow**


Configuring argo
Argo is the software that will control our kubernetes cluster. You will need to install it on the cluster.
https://argoproj.github.io/argo/quick-start/
Move to /bin/ (or wherever kubectl sits)

`$ sudo curl -sSL -o /bin/argo https://github.com/argoproj/argo/releases/download/v2.2.1/argo-linux-amd64`

`$ sudo chmod +x /bin/argo`

You will also need to install Argo on the kubernetes cluster:

`$ kubectl create ns argo`

`$ kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo/stable/manifests/quick-start-postgres.yaml`

Argo is namespaced, so any argo or kubectl commands need the '-n argo' selector or you won't see the right results.

Pipelines
Pipelines are based on workflows and parameters files. The workflows are broken into a series of steps. For each step, the data are pulled from S3, unzipped and untarred, and processed. The results are then zipped and tarred, and copied back to S3.  These transfers can be time-intensive, so the workflows are configured to minimize unnecessary transfers, while still separating operations to be more failsafe. The worflows are iterated over each of the samples identified in the parameters file. Upload these files to the bastion host (via scp).


**Advice**
- It is strongly advised that your first submitted job be a test run on one node, using only the largest sample file(s). This will help you set the right disk size in the nodes and within the workflow.
- Each pipeline consists of multiple pieces. You should revise the nodes (# CPUs/RAM) as appropriate for each step, e.g., lots of CPUs for alignment, fewer with more RAM for rest of pipeline.
- Items stored in Kubernetes itself that are needed by your workflow should be in the same namespace as your workflow
- Check the log for your initial pods in PodInitializing state to catch early problems with configuration.
REMEMBER THAT RUNNING NODES COST THE INVESTIGATOR MONEY. Check in on test runs early and often (at LEAST once an hour), and check larger jobs regulalry for progress (every 4-8 hours).

-------
**Using argo**

`$ argo -n argo submit gatk-pre.workflow --parameter-file gatk-pre.params`

`$ argo -n argo get workflow `          # workfolw will be "gatk-pre-randomchars", or if there's just one use '@latest'

`$ argo -n argo logs workflow `          # review error logs. You may wish to save these to a text file for reference.

`$ argo -n argo delete workflow `         # delete a failed or completed workflow


Useful commands:

##### list all pods with Kubernetes status. If there's a problem with the Argo process, you may see it here

`$ kubectl -n argo get pods`
##### to see which node each pod is using

`$ kubectl -n argo get pods -o wide`

##### see Pod status + Events, can tell you why pod state has not progressed

`$ kubectl -n argo describe pod/$PODNAME`

##### get logfile from init, wait or main container
- init container - set up for run, pull data in
- wait container - moves data into correct spot, waits for pod completion and moves data out
- main container - the container from your workflow. Same as Argo log command

`$ kubectl -n argo logs $PODNAME $CONTAINER`

##### track pods completed. Mostly useful for single-step workflows

`$ kubectl -n argo get pods | grep $WORKFLOW_NAME | grep -c 'Completed'`


